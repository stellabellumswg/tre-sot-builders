@echo off
echo Welcome this batch file will build your server data folder into tres!
:Ask
echo Would you like to build the server tre files?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto build 
If /I "%INPUT%"=="n" goto end

:build
del data_compressed_3D.rsp
del data_compressed_other.rsp
del data_compressed_2D.rsp
del data_uncompressed_AudioVideo.rsp

..\..\tools\TreeFileRspBuilder_r TreBuilderRSP-Server.cfg
..\..\tools\TreeFileBuilder_r -r data_compressed_other.rsp ..\..\tres\Server.sot

del data_compressed_3D.rsp
del data_compressed_other.rsp
del data_compressed_2D.rsp
del data_uncompressed_AudioVideo.rsp

..\..\tools\TreeFileRspBuilder_r TreBuilderRSP-Shared.cfg
..\..\tools\TreeFileBuilder_r -r data_compressed_other.rsp ..\..\tres\Shared.sot

del data_compressed_3D.rsp
del data_compressed_other.rsp
del data_compressed_2D.rsp
del data_uncompressed_AudioVideo.rsp

..\..\tools\TreeFileRspBuilder_r TreBuilderRSP.cfg
..\..\tools\TreeFileBuilder -r data_compressed_3D.rsp ..\..\tres\3DAssets.sot
..\..\tools\TreeFileBuilder -r data_compressed_other.rsp ..\..\tres\Misc.sot
..\..\tools\TreeFileBuilder -r data_compressed_2D.rsp ..\..\tres\2DAssets.sot
..\..\tools\TreeFileBuilder -r data_uncompressed_AudioVideo.rsp -f ..\..\tres\AV.sot

del data_compressed_3D.rsp
del data_compressed_other.rsp
del data_compressed_2D.rsp
del data_uncompressed_AudioVideo.rsp
echo Be sure you change your servercommon.cfg [SharedFile] search paths to use new /tres folder instead of /data -Erusman
echo You are all finished!
:end
echo May the Force be with you! -Erusman 
pause

