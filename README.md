# TRE/SOT Builder Repository

The basic process is that we build .rsp file lists of what we want to compress based on the config files, and then compress them into a tre or sot file. Thanks to Erusman for sharing this trick.

# Useful Changes Appreciated

If you make any useful changes or improvements, put in a pull request/merge request and I'll merge it in, and you'll become a contributor!

# Usage

## Linux

You need to install wine and xvfb, and any other libs that are complained about along the way.

## Windows

Use and/or modify the included batch file.

## Configuration

See the included cfg files for the necessary paths to build a given archive. Notice the structure is important.

# Buy Darth A Caffinated Beverage

bitcoin:16e1QRRmnBmod3RLtdDMa5muKBGRXE3Kmh
