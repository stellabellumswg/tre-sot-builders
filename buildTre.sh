#!/bin/bash

killall Xvfb &> /dev/null
Xvfb :1 -screen 0 800x600x8 &
DISPLAY=127.0.0.1:1.0
export DISPLAY

delfiles(){
	rm data_compressed_3D.rsp &> /dev/null
	rm data_compressed_other.rsp &> /dev/null
	rm data_compressed_2D.rsp &> /dev/null
	rm data_uncompressed_AudioVideo.rsp &> /dev/null
}

if [ -d "../data/sku.0/sys.server/compiled/game/script" ]; then
	rm -rf ./script &> /dev/null 
	mv ../data/sku.0/sys.server/compiled/game/script ./
fi

delfiles

/usr/bin/wine TreeFileRspBuilder_r.exe TreBuilderRSP-Server.cfg
./TreeFileBuilder -r data_compressed_other.rsp Server.sot

delfiles

/usr/bin/wine TreeFileRspBuilder_r.exe TreBuilderRSP-Shared.cfg
./TreeFileBuilder -r data_compressed_other.rsp Shared.sot

delfiles

/usr/bin/wine TreeFileRspBuilder_r.exe TreBuilderRSP.cfg
./TreeFileBuilder -r data_compressed_3D.rsp 3DAssets.sot
./TreeFileBuilder -r data_compressed_other.rsp Misc.sot
./TreeFileBuilder -r data_compressed_2D.rsp 2DAssets.sot
./TreeFileBuilder -r data_uncompressed_AudioVideo.rsp -f AV.sot
